webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/country.js":
/*!*******************************!*\
  !*** ./components/country.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Country; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/zed/Documents/CountryDataProject/components/country.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

var buttonClassStyles = {
  width: '100%',
  padding: '10px',
  border: '2px solid #495ad8',
  color: '#495ad8',
  backgroundColor: '#fff'
};
var img = {
  height: '100%',
  width: '100%'
};
var ul = {
  paddingLeft: '0'
};
var li = {
  marginTop: '20px',
  padding: '10px',
  listStyleType: 'none',
  boxShadow: 'rgb(193, 193, 193) 0px 0px 2px 1px'
};
var h1 = {
  fontWeight: 'bolder'
};

function utc(timezone) {
  var cdt = new Date();
  var udt = d.toUTCString();
}

;

var CountryTemplate = function CountryTemplate(props) {
  return __jsx("li", {
    style: li,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  }, __jsx("div", {
    className: "container-fluid",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-4 col-md-4 col-sm-12 col-xs-12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  }, __jsx("img", {
    src: props.country.flag,
    style: img,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38
    },
    __self: this
  })), __jsx("div", {
    className: "col-lg-8 col-md-8 col-sm-12 col-xs-12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, __jsx("h1", {
    style: h1,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, props.country.name), __jsx("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, __jsx("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, "Currency : ", props.country.currencies[0].name)), __jsx("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, __jsx("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, "Current date and time : ", props.country.timezones[0])), __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
    href: 'http://www.google.com/maps/place/' + props.country.latlng[0] + ',' + props.country.latlng[1],
    target: "_blank",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, __jsx("a", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, "About Us")))))));
};

function Country(props) {
  return __jsx("ul", {
    style: ul,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, props.countries.map(function (country) {
    return __jsx(CountryTemplate, {
      key: country.name.toString(),
      country: country,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58
      },
      __self: this
    });
  }));
}

/***/ })

})
//# sourceMappingURL=index.js.dc6e72cb918d01bfd943.hot-update.js.map