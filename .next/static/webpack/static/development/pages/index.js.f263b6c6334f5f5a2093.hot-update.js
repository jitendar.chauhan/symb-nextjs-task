webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "/home/zed/Documents/CountryDataProject/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


var buttonClassStyles = {
  width: '100%',
  padding: '10px',
  border: '1px solid #ccc'
};

var Index = function Index(props) {
  return __jsx("html", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }, __jsx("head", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, __jsx("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, "Task"), __jsx("link", {
    rel: "stylesheet",
    href: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",
    integrity: "sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh",
    crossOrigin: "anonymous",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  })), __jsx("body", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, __jsx("div", {
    className: "container-fluid",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }), __jsx("div", {
    className: "col-lg-6 col-md-6 col-sm-12 col-xs-12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, __jsx("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, "Country"), __jsx("input", {
    type: "text",
    className: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  })), props.countries.map(function (show) {
    return __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, __jsx("div", {
      "class": "container-fluid",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, __jsx("div", {
      "class": "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, __jsx("div", {
      "class": "col-lg-4 col-md-4 col-sm-12 col-xs-12",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, __jsx("img", {
      src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEVWPXz///9TOXry8PVFJHFoUopuWo1wXo5KLHRPM3f9/P2flLJ0YZLFvtHJw9RQNXhLLnVIKXNbQ4Da1eHm4+uvpb+CcZ2ajq9rVoxeRoLu7PKEdZ739vnTzdxhSoSThqm6sshBHm6nnLmHeKCzqcJ7apjV0N2/uMzh3eeNf6WLf6+FAAAHuElEQVR4nO2df5OqLBSAAc0LaSFpaaXmdrcf+/2/4Kt1260N3PYNPMj0/NsM0zMgKJxzQLgLr6yzRVDNkJ3MqmAR1qXX6YCUvxRllgsac0KgRToghMfUz7MOS5VhslkKbrPbNYSL2Sb5leE0j9lQ9M4QFo+mDxtOczEsvTNEVDLHe8N0N0i/FiJ26c+Gc86h/+gTcDb/wdCLKPSffJI48roMEzLkDjzDSaI2nAxsApVD4onKsBbQf04TtJYb1m/Q/0wbb7XMcDL0OeYaOrk3TBj0v9IKS74bela/YP8egrxvhtHwl4lbeHRrOI+h/5F24vm1YerWQ3iGp1eGO9fGaAvffRlOXVnqbxHTT8ORW/PoBTK6GDrahf86sTXM3ezCphPzs2Hi3kpxIU5OhhsXl4ozfNMaFktXB2kzTJdFY1i6Os+0iLIxzFxc7S+wrDF0diZtaWZT5Lk8SBHyPVS69Gl/Dy1R7e5q2BLXKHR5ommmmhAtXJ5omqlmgQLHDQNUQf8Hw1TI1jN6Xbju9+LFixcvXrx48QBEA9AOnZDgSfJRVc24EDGzNA5SrMZPUhSFt0r3h+3HSFALo3n87ujc31GU21zElklqNTxZHnbcqp7UbtiQbitqj6MJw6Yj65k1+39mDBu2zJIdQGOGeLW2Y6PanCHGtRXTqklDfJxZMFKNGuIigD+cNmuIxzl4Lxo2xF4F/SyaNsQpdCcaN8QT4LXfvCFew/ZiD4ae77oh3oKO0z4MPdDpVGVYTh9inxxXPytmkOu+ynAp6CMIytBoXf9gCRpYqDL8RUQu4bGfH8ZdipDn8BoMWxTJnxe2gMNUk2HruFEb7gE/FbUZIsR2ypE6BnwQNRoi9lfZiYCxhToNkZjIG8N4A/fmptWQVKpxChh6p9XwOmfwlqkrhnyhMEzgQmD1GiKiaC51xlDs5c0BfkFpNozv6iG4ZsgyeXOFM4aqqcadPiSBvLmVMzPNOV/wnqM7hpW8OcCEiZ768ODKO43yOQTMzNI9lyo+oAC3MXSvh6G0NVe+gBvig7S1xI1djBYh31eE3DDVa0giaWNjyGoBeg3jd2ljoOmDencxZoW0MdDzNb07UbW0LcAPfKTXkMufQuCyKxoNv6oe3QJcs0OfIa/kK0UBHCWtzZBGiiO2NXDQkCZD7stf16CPuJEeQ8Lo+r7o6JkDeK0ApSF/LBGBM8ryUOWHJ/BlDlWGi+gRdotNOFHqYVxbEGKqjMXoPLV+kAz6GWwxGG2yCmwQNGh4sKSasSnDZGfBI3jCjOFxbUWI9wkjhhvftydtxkwfrspsJOx4DM3NNOP9hw8fxo7MxiaustiCfjQbfbn6C1/g33R86RQ8qcR4BK0XuR/JvoFd+/uIgs5AFfswhP3G6MUQ7yw8t9iGj7Kd1+VKvtf9yXhm3/lhxR4npmK5fu/41Md4P/hIBcJjtuuK9Iarkqtzz5tWqiupMC7AxqnesyflrinG71Dzqe5TbmU0O1itY82GiAUqRai7G3QbIqYKE4YKT9RuiKgixBR/wHxl6DdETHEIBXSOaMCQfyiGKcxUY8AQvSk6EWaYmjC8v/zuDMySaMLwdGODhCPId6IJQ8Tlw9QDKSZrxJCW8kZBYjCNGKqyLv5CTDVmRqki6yKE+IQyY6iIFAaZTM0YruWNguToGTEkO3mjIAnPZgwVfbh3pg9VzyFIbQUzhorU/KMzhmzreh9Sxb6iO4ZMsTvszFxKlvI2YW7zNfJ9qKoBMnflrc1XbX2D1I4wYKjKQcQ4cuTryVed0Hgg2976DbnipdSZXQxClAeJjuxECfUpIkz6k27DN3mKZUsBc0iqOf+QdZwDA+VzazWkwVEtCLNW6DQkMVGdOp1IB326RnhMq7o75ATocO3Zuok0plQwlK/rrvF56kKoYAyV4XTyINMySR8ofwnWhT1FfUEmO/dkWCCwsK+eDAHTLPsxnFtYNUIrILsXfRpOQMtd92B4cL2e9xY4ic20YbGAztIzbFgS8Nwno4arjQX3Phk0HG/hOxAZNCzm3IpEZ1OGacjs8DNjuKqj2IbxeUa7YfoeEXvSnJFWw3GR1B/Et0qvwf8hn+cBL2+VHqfv4TqPhQ1Zsd8hwZ8nOF1DOiONmrXXkOq4Sxba4MWLFy9ePARICHyPzFAF/RcMUyHI+6J6gARo4bjhAoUWvu9qhIUI8CKePohrBFqI2Dy0RB54fUmjCA9B3vllHpJjBHtHpGlY1hiWLg9TUTaGYPUKeoAsi8YQsHKIcdrsFAR8lalZ4uRk6O5sesqabg2B656b4xSQ2xo+l+NiL+e4/5Oho514jqk+GQJfQGCIf9e4nQ1TJw3TK0PQyCpDXErd/DPEkWu9+HkjyMXQc+yQ5OtGkIshTtx6d2OfKXCfhrAxgLq5uiD6yxDX8Jct6OLt6t6hK0Ncw1cm1gO9vljp2hBP7CmG/gSE3Vx/fWOIEzT8RYOT2zzbW0PsRUOfb2j0LQTmm2HzdmPJzRL/D87v0qfuDHG6GOyEQ8TuPgf13rD5mBoN0pGIkSyBUWbYOAbU2hgeOYTHuTxBU27YzKqbSgxm7SBMLDeqSgUqQ4yLMst9GjOrX8kJaYtJB1mpjrBTG7Z4ZR0ugsrWs/5ZFSzCuuyOkPwPMlmANsdr66IAAAAASUVORK5CYII=",
      style: "height: 100%;width: 100%;",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    })), __jsx("div", {
      "class": "col-lg-8 col-md-8 col-sm-12 col-xs-12",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40
      },
      __self: this
    }, __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, "Country : India")), __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Current time and date : 12mt, isef , ugiwue.")), __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, "Country : India")), __jsx("button", {
      style: buttonClassStyles,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    })))));
  })), __jsx("div", {
    "class": "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  })))));
};

Index.getInitialProps = function _callee() {
  var res, data;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_3___default()('https://restcountries.eu/rest/v2/all'));

        case 2:
          res = _context.sent;
          _context.next = 5;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res.json());

        case 5:
          data = _context.sent;
          console.log("Show data fetched. Count: ".concat(data.length));
          return _context.abrupt("return", {
            countries: data.map(function (entry) {
              return entry;
            })
          });

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ })

})
//# sourceMappingURL=index.js.f263b6c6334f5f5a2093.hot-update.js.map