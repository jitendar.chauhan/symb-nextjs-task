webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_country__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/country */ "./components/country.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "/home/zed/Documents/CountryDataProject/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



var countries = [];
var h1 = {
  fontWeight: 'bolder'
};
var input = {
  width: '100%',
  padding: '10px',
  backgroundImage: 'url(https://www.thesirona.com/themes/storefront/public/images/search.png?v=1.1.5)',
  backgroundSize: '25px 25px',
  backgroundPosition: '98% center',
  backgroundRepeat: 'no-repeat',
  border: '1px solid #ccc'
};

var Index = function Index(props) {
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx("link", {
    rel: "stylesheet",
    href: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",
    integrity: "sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh",
    crossOrigin: "anonymous",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }), __jsx("div", {
    className: "container-fluid",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, " "), __jsx("div", {
    className: "col-lg-6 col-md-6 col-sm-12 col-xs-12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, __jsx("h1", {
    style: h1,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, "Country"), __jsx("input", {
    type: "text",
    style: input,
    onInput: function onInput(e) {
      return Index.searchCountry(e.currentTarget.value);
    },
    placeholder: "Search countries",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  })), __jsx("div", {
    id: "dataDump",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, __jsx(_components_country__WEBPACK_IMPORTED_MODULE_3__["default"], {
    countries: props.countries,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }))), __jsx("div", {
    className: "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, " "))));
};

Index.getInitialProps = function _callee() {
  var res, data;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()('https://restcountries.eu/rest/v2/all'));

        case 2:
          res = _context.sent;
          _context.next = 5;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res.json());

        case 5:
          data = _context.sent;
          countries = data.map(function (entry) {
            return entry;
          });
          return _context.abrupt("return", {
            countries: countries
          });

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
};

Index.find = function (str) {
  var filtered = function filtered(arr, query) {
    return arr.filter(function (el) {
      return el.name.includes(str);
    });
  };

  return filtered(countries, str);
};

Index.searchCountry = function (value) {
  var t = Index.find(value);
  Index.render(Index.find(value));
};

Index.render = function (value) {
  react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.render(__jsx(_components_country__WEBPACK_IMPORTED_MODULE_3__["default"], {
    countries: value,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }), document.getElementById('dataDump'));
};

Index.getInitialProps();
/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ })

})
//# sourceMappingURL=index.js.a8b8dd963ecad6760d40.hot-update.js.map