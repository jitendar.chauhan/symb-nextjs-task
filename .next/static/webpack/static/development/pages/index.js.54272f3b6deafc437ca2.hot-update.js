webpackHotUpdate("static/development/pages/index.js",{

/***/ "./node_modules/string-hash/index.js":
/*!*******************************************!*\
  !*** ./node_modules/string-hash/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function hash(str) {
  var hash = 5381,
      i    = str.length;

  while(i) {
    hash = (hash * 33) ^ str.charCodeAt(--i);
  }

  /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
   * integers. Since we want the results to be always positive, convert the
   * signed int to an unsigned by doing an unsigned bitshift. */
  return hash >>> 0;
}

module.exports = hash;


/***/ }),

/***/ "./node_modules/styled-jsx/dist/lib/stylesheet.js":
/*!********************************************************!*\
  !*** ./node_modules/styled-jsx/dist/lib/stylesheet.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

exports.__esModule = true;
exports["default"] = void 0;

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
Based on Glamor's sheet
https://github.com/threepointone/glamor/blob/667b480d31b3721a905021b26e1290ce92ca2879/src/sheet.js
*/
var isProd = typeof process !== 'undefined' && process.env && "development" === 'production';

var isString = function isString(o) {
  return Object.prototype.toString.call(o) === '[object String]';
};

var StyleSheet =
/*#__PURE__*/
function () {
  function StyleSheet(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        _ref$name = _ref.name,
        name = _ref$name === void 0 ? 'stylesheet' : _ref$name,
        _ref$optimizeForSpeed = _ref.optimizeForSpeed,
        optimizeForSpeed = _ref$optimizeForSpeed === void 0 ? isProd : _ref$optimizeForSpeed,
        _ref$isBrowser = _ref.isBrowser,
        isBrowser = _ref$isBrowser === void 0 ? typeof window !== 'undefined' : _ref$isBrowser;

    invariant(isString(name), '`name` must be a string');
    this._name = name;
    this._deletedRulePlaceholder = "#" + name + "-deleted-rule____{}";
    invariant(typeof optimizeForSpeed === 'boolean', '`optimizeForSpeed` must be a boolean');
    this._optimizeForSpeed = optimizeForSpeed;
    this._isBrowser = isBrowser;
    this._serverSheet = undefined;
    this._tags = [];
    this._injected = false;
    this._rulesCount = 0;
    var node = this._isBrowser && document.querySelector('meta[property="csp-nonce"]');
    this._nonce = node ? node.getAttribute('content') : null;
  }

  var _proto = StyleSheet.prototype;

  _proto.setOptimizeForSpeed = function setOptimizeForSpeed(bool) {
    invariant(typeof bool === 'boolean', '`setOptimizeForSpeed` accepts a boolean');
    invariant(this._rulesCount === 0, 'optimizeForSpeed cannot be when rules have already been inserted');
    this.flush();
    this._optimizeForSpeed = bool;
    this.inject();
  };

  _proto.isOptimizeForSpeed = function isOptimizeForSpeed() {
    return this._optimizeForSpeed;
  };

  _proto.inject = function inject() {
    var _this = this;

    invariant(!this._injected, 'sheet already injected');
    this._injected = true;

    if (this._isBrowser && this._optimizeForSpeed) {
      this._tags[0] = this.makeStyleTag(this._name);
      this._optimizeForSpeed = 'insertRule' in this.getSheet();

      if (!this._optimizeForSpeed) {
        if (!isProd) {
          console.warn('StyleSheet: optimizeForSpeed mode not supported falling back to standard mode.');
        }

        this.flush();
        this._injected = true;
      }

      return;
    }

    this._serverSheet = {
      cssRules: [],
      insertRule: function insertRule(rule, index) {
        if (typeof index === 'number') {
          _this._serverSheet.cssRules[index] = {
            cssText: rule
          };
        } else {
          _this._serverSheet.cssRules.push({
            cssText: rule
          });
        }

        return index;
      },
      deleteRule: function deleteRule(index) {
        _this._serverSheet.cssRules[index] = null;
      }
    };
  };

  _proto.getSheetForTag = function getSheetForTag(tag) {
    if (tag.sheet) {
      return tag.sheet;
    } // this weirdness brought to you by firefox


    for (var i = 0; i < document.styleSheets.length; i++) {
      if (document.styleSheets[i].ownerNode === tag) {
        return document.styleSheets[i];
      }
    }
  };

  _proto.getSheet = function getSheet() {
    return this.getSheetForTag(this._tags[this._tags.length - 1]);
  };

  _proto.insertRule = function insertRule(rule, index) {
    invariant(isString(rule), '`insertRule` accepts only strings');

    if (!this._isBrowser) {
      if (typeof index !== 'number') {
        index = this._serverSheet.cssRules.length;
      }

      this._serverSheet.insertRule(rule, index);

      return this._rulesCount++;
    }

    if (this._optimizeForSpeed) {
      var sheet = this.getSheet();

      if (typeof index !== 'number') {
        index = sheet.cssRules.length;
      } // this weirdness for perf, and chrome's weird bug
      // https://stackoverflow.com/questions/20007992/chrome-suddenly-stopped-accepting-insertrule


      try {
        sheet.insertRule(rule, index);
      } catch (error) {
        if (!isProd) {
          console.warn("StyleSheet: illegal rule: \n\n" + rule + "\n\nSee https://stackoverflow.com/q/20007992 for more info");
        }

        return -1;
      }
    } else {
      var insertionPoint = this._tags[index];

      this._tags.push(this.makeStyleTag(this._name, rule, insertionPoint));
    }

    return this._rulesCount++;
  };

  _proto.replaceRule = function replaceRule(index, rule) {
    if (this._optimizeForSpeed || !this._isBrowser) {
      var sheet = this._isBrowser ? this.getSheet() : this._serverSheet;

      if (!rule.trim()) {
        rule = this._deletedRulePlaceholder;
      }

      if (!sheet.cssRules[index]) {
        // @TBD Should we throw an error?
        return index;
      }

      sheet.deleteRule(index);

      try {
        sheet.insertRule(rule, index);
      } catch (error) {
        if (!isProd) {
          console.warn("StyleSheet: illegal rule: \n\n" + rule + "\n\nSee https://stackoverflow.com/q/20007992 for more info");
        } // In order to preserve the indices we insert a deleteRulePlaceholder


        sheet.insertRule(this._deletedRulePlaceholder, index);
      }
    } else {
      var tag = this._tags[index];
      invariant(tag, "old rule at index `" + index + "` not found");
      tag.textContent = rule;
    }

    return index;
  };

  _proto.deleteRule = function deleteRule(index) {
    if (!this._isBrowser) {
      this._serverSheet.deleteRule(index);

      return;
    }

    if (this._optimizeForSpeed) {
      this.replaceRule(index, '');
    } else {
      var tag = this._tags[index];
      invariant(tag, "rule at index `" + index + "` not found");
      tag.parentNode.removeChild(tag);
      this._tags[index] = null;
    }
  };

  _proto.flush = function flush() {
    this._injected = false;
    this._rulesCount = 0;

    if (this._isBrowser) {
      this._tags.forEach(function (tag) {
        return tag && tag.parentNode.removeChild(tag);
      });

      this._tags = [];
    } else {
      // simpler on server
      this._serverSheet.cssRules = [];
    }
  };

  _proto.cssRules = function cssRules() {
    var _this2 = this;

    if (!this._isBrowser) {
      return this._serverSheet.cssRules;
    }

    return this._tags.reduce(function (rules, tag) {
      if (tag) {
        rules = rules.concat(Array.prototype.map.call(_this2.getSheetForTag(tag).cssRules, function (rule) {
          return rule.cssText === _this2._deletedRulePlaceholder ? null : rule;
        }));
      } else {
        rules.push(null);
      }

      return rules;
    }, []);
  };

  _proto.makeStyleTag = function makeStyleTag(name, cssString, relativeToTag) {
    if (cssString) {
      invariant(isString(cssString), 'makeStyleTag acceps only strings as second parameter');
    }

    var tag = document.createElement('style');
    if (this._nonce) tag.setAttribute('nonce', this._nonce);
    tag.type = 'text/css';
    tag.setAttribute("data-" + name, '');

    if (cssString) {
      tag.appendChild(document.createTextNode(cssString));
    }

    var head = document.head || document.getElementsByTagName('head')[0];

    if (relativeToTag) {
      head.insertBefore(tag, relativeToTag);
    } else {
      head.appendChild(tag);
    }

    return tag;
  };

  _createClass(StyleSheet, [{
    key: "length",
    get: function get() {
      return this._rulesCount;
    }
  }]);

  return StyleSheet;
}();

exports["default"] = StyleSheet;

function invariant(condition, message) {
  if (!condition) {
    throw new Error("StyleSheet: " + message + ".");
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/styled-jsx/dist/style.js":
/*!***********************************************!*\
  !*** ./node_modules/styled-jsx/dist/style.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.flush = flush;
exports["default"] = void 0;

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _stylesheetRegistry = _interopRequireDefault(__webpack_require__(/*! ./stylesheet-registry */ "./node_modules/styled-jsx/dist/stylesheet-registry.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _inheritsLoose(subClass, superClass) { subClass.prototype = Object.create(superClass.prototype); subClass.prototype.constructor = subClass; subClass.__proto__ = superClass; }

var styleSheetRegistry = new _stylesheetRegistry["default"]();

var JSXStyle =
/*#__PURE__*/
function (_Component) {
  _inheritsLoose(JSXStyle, _Component);

  function JSXStyle(props) {
    var _this;

    _this = _Component.call(this, props) || this;
    _this.prevProps = {};
    return _this;
  }

  JSXStyle.dynamic = function dynamic(info) {
    return info.map(function (tagInfo) {
      var baseId = tagInfo[0];
      var props = tagInfo[1];
      return styleSheetRegistry.computeId(baseId, props);
    }).join(' ');
  } // probably faster than PureComponent (shallowEqual)
  ;

  var _proto = JSXStyle.prototype;

  _proto.shouldComponentUpdate = function shouldComponentUpdate(otherProps) {
    return this.props.id !== otherProps.id || // We do this check because `dynamic` is an array of strings or undefined.
    // These are the computed values for dynamic styles.
    String(this.props.dynamic) !== String(otherProps.dynamic);
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    styleSheetRegistry.remove(this.props);
  };

  _proto.render = function render() {
    // This is a workaround to make the side effect async safe in the "render" phase.
    // See https://github.com/zeit/styled-jsx/pull/484
    if (this.shouldComponentUpdate(this.prevProps)) {
      // Updates
      if (this.prevProps.id) {
        styleSheetRegistry.remove(this.prevProps);
      }

      styleSheetRegistry.add(this.props);
      this.prevProps = this.props;
    }

    return null;
  };

  return JSXStyle;
}(_react.Component);

exports["default"] = JSXStyle;

function flush() {
  var cssRules = styleSheetRegistry.cssRules();
  styleSheetRegistry.flush();
  return cssRules;
}

/***/ }),

/***/ "./node_modules/styled-jsx/dist/stylesheet-registry.js":
/*!*************************************************************!*\
  !*** ./node_modules/styled-jsx/dist/stylesheet-registry.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports["default"] = void 0;

var _stringHash = _interopRequireDefault(__webpack_require__(/*! string-hash */ "./node_modules/string-hash/index.js"));

var _stylesheet = _interopRequireDefault(__webpack_require__(/*! ./lib/stylesheet */ "./node_modules/styled-jsx/dist/lib/stylesheet.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var sanitize = function sanitize(rule) {
  return rule.replace(/\/style/gi, '\\/style');
};

var StyleSheetRegistry =
/*#__PURE__*/
function () {
  function StyleSheetRegistry(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        _ref$styleSheet = _ref.styleSheet,
        styleSheet = _ref$styleSheet === void 0 ? null : _ref$styleSheet,
        _ref$optimizeForSpeed = _ref.optimizeForSpeed,
        optimizeForSpeed = _ref$optimizeForSpeed === void 0 ? false : _ref$optimizeForSpeed,
        _ref$isBrowser = _ref.isBrowser,
        isBrowser = _ref$isBrowser === void 0 ? typeof window !== 'undefined' : _ref$isBrowser;

    this._sheet = styleSheet || new _stylesheet["default"]({
      name: 'styled-jsx',
      optimizeForSpeed: optimizeForSpeed
    });

    this._sheet.inject();

    if (styleSheet && typeof optimizeForSpeed === 'boolean') {
      this._sheet.setOptimizeForSpeed(optimizeForSpeed);

      this._optimizeForSpeed = this._sheet.isOptimizeForSpeed();
    }

    this._isBrowser = isBrowser;
    this._fromServer = undefined;
    this._indices = {};
    this._instancesCounts = {};
    this.computeId = this.createComputeId();
    this.computeSelector = this.createComputeSelector();
  }

  var _proto = StyleSheetRegistry.prototype;

  _proto.add = function add(props) {
    var _this = this;

    if (undefined === this._optimizeForSpeed) {
      this._optimizeForSpeed = Array.isArray(props.children);

      this._sheet.setOptimizeForSpeed(this._optimizeForSpeed);

      this._optimizeForSpeed = this._sheet.isOptimizeForSpeed();
    }

    if (this._isBrowser && !this._fromServer) {
      this._fromServer = this.selectFromServer();
      this._instancesCounts = Object.keys(this._fromServer).reduce(function (acc, tagName) {
        acc[tagName] = 0;
        return acc;
      }, {});
    }

    var _this$getIdAndRules = this.getIdAndRules(props),
        styleId = _this$getIdAndRules.styleId,
        rules = _this$getIdAndRules.rules; // Deduping: just increase the instances count.


    if (styleId in this._instancesCounts) {
      this._instancesCounts[styleId] += 1;
      return;
    }

    var indices = rules.map(function (rule) {
      return _this._sheet.insertRule(rule);
    }) // Filter out invalid rules
    .filter(function (index) {
      return index !== -1;
    });
    this._indices[styleId] = indices;
    this._instancesCounts[styleId] = 1;
  };

  _proto.remove = function remove(props) {
    var _this2 = this;

    var _this$getIdAndRules2 = this.getIdAndRules(props),
        styleId = _this$getIdAndRules2.styleId;

    invariant(styleId in this._instancesCounts, "styleId: `" + styleId + "` not found");
    this._instancesCounts[styleId] -= 1;

    if (this._instancesCounts[styleId] < 1) {
      var tagFromServer = this._fromServer && this._fromServer[styleId];

      if (tagFromServer) {
        tagFromServer.parentNode.removeChild(tagFromServer);
        delete this._fromServer[styleId];
      } else {
        this._indices[styleId].forEach(function (index) {
          return _this2._sheet.deleteRule(index);
        });

        delete this._indices[styleId];
      }

      delete this._instancesCounts[styleId];
    }
  };

  _proto.update = function update(props, nextProps) {
    this.add(nextProps);
    this.remove(props);
  };

  _proto.flush = function flush() {
    this._sheet.flush();

    this._sheet.inject();

    this._fromServer = undefined;
    this._indices = {};
    this._instancesCounts = {};
    this.computeId = this.createComputeId();
    this.computeSelector = this.createComputeSelector();
  };

  _proto.cssRules = function cssRules() {
    var _this3 = this;

    var fromServer = this._fromServer ? Object.keys(this._fromServer).map(function (styleId) {
      return [styleId, _this3._fromServer[styleId]];
    }) : [];

    var cssRules = this._sheet.cssRules();

    return fromServer.concat(Object.keys(this._indices).map(function (styleId) {
      return [styleId, _this3._indices[styleId].map(function (index) {
        return cssRules[index].cssText;
      }).join(_this3._optimizeForSpeed ? '' : '\n')];
    }) // filter out empty rules
    .filter(function (rule) {
      return Boolean(rule[1]);
    }));
  }
  /**
   * createComputeId
   *
   * Creates a function to compute and memoize a jsx id from a basedId and optionally props.
   */
  ;

  _proto.createComputeId = function createComputeId() {
    var cache = {};
    return function (baseId, props) {
      if (!props) {
        return "jsx-" + baseId;
      }

      var propsToString = String(props);
      var key = baseId + propsToString; // return `jsx-${hashString(`${baseId}-${propsToString}`)}`

      if (!cache[key]) {
        cache[key] = "jsx-" + (0, _stringHash["default"])(baseId + "-" + propsToString);
      }

      return cache[key];
    };
  }
  /**
   * createComputeSelector
   *
   * Creates a function to compute and memoize dynamic selectors.
   */
  ;

  _proto.createComputeSelector = function createComputeSelector(selectoPlaceholderRegexp) {
    if (selectoPlaceholderRegexp === void 0) {
      selectoPlaceholderRegexp = /__jsx-style-dynamic-selector/g;
    }

    var cache = {};
    return function (id, css) {
      // Sanitize SSR-ed CSS.
      // Client side code doesn't need to be sanitized since we use
      // document.createTextNode (dev) and the CSSOM api sheet.insertRule (prod).
      if (!this._isBrowser) {
        css = sanitize(css);
      }

      var idcss = id + css;

      if (!cache[idcss]) {
        cache[idcss] = css.replace(selectoPlaceholderRegexp, id);
      }

      return cache[idcss];
    };
  };

  _proto.getIdAndRules = function getIdAndRules(props) {
    var _this4 = this;

    var css = props.children,
        dynamic = props.dynamic,
        id = props.id;

    if (dynamic) {
      var styleId = this.computeId(id, dynamic);
      return {
        styleId: styleId,
        rules: Array.isArray(css) ? css.map(function (rule) {
          return _this4.computeSelector(styleId, rule);
        }) : [this.computeSelector(styleId, css)]
      };
    }

    return {
      styleId: this.computeId(id),
      rules: Array.isArray(css) ? css : [css]
    };
  }
  /**
   * selectFromServer
   *
   * Collects style tags from the document with id __jsx-XXX
   */
  ;

  _proto.selectFromServer = function selectFromServer() {
    var elements = Array.prototype.slice.call(document.querySelectorAll('[id^="__jsx-"]'));
    return elements.reduce(function (acc, element) {
      var id = element.id.slice(2);
      acc[id] = element;
      return acc;
    }, {});
  };

  return StyleSheetRegistry;
}();

exports["default"] = StyleSheetRegistry;

function invariant(condition, message) {
  if (!condition) {
    throw new Error("StyleSheetRegistry: " + message + ".");
  }
}

/***/ }),

/***/ "./node_modules/styled-jsx/style.js":
/*!******************************************!*\
  !*** ./node_modules/styled-jsx/style.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/style */ "./node_modules/styled-jsx/dist/style.js")


/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "/home/zed/Documents/CountryDataProject/pages/index.js";


var __jsx = react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement;



var Index = function Index(props) {
  return __jsx("html", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, __jsx("head", {
    className: "jsx-1878896370",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, __jsx("title", {
    className: "jsx-1878896370",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, "Task"), __jsx("link", {
    rel: "stylesheet",
    href: "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",
    integrity: "sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh",
    crossOrigin: "anonymous",
    className: "jsx-1878896370",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "1878896370",
    __self: this
  }, ".button.jsx-1878896370{width:100%;padding:10px;border:1px solid #ccc;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3plZC9Eb2N1bWVudHMvQ291bnRyeURhdGFQcm9qZWN0L3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVVvQixBQUd3QixXQUNFLGFBQ1Msc0JBQzFCIiwiZmlsZSI6Ii9ob21lL3plZC9Eb2N1bWVudHMvQ291bnRyeURhdGFQcm9qZWN0L3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJztcbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLXVuZmV0Y2gnO1xuXG5jb25zdCBJbmRleCA9IHByb3BzID0+IChcbiAgICA8aHRtbD5cbiAgICA8aGVhZD5cbiAgICAgICAgPHRpdGxlPlRhc2s8L3RpdGxlPlxuICAgICAgICA8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cImh0dHBzOi8vc3RhY2twYXRoLmJvb3RzdHJhcGNkbi5jb20vYm9vdHN0cmFwLzQuNC4xL2Nzcy9ib290c3RyYXAubWluLmNzc1wiXG4gICAgICAgICAgICAgIGludGVncml0eT1cInNoYTM4NC1Wa29vOHg0Q0dzTzMrSGh4djhUL1E1UGFYdGtLdHU2dWc1VE9lTlY2Z0JpRmVXUEdGTjlNdWhPZjIzUTlJZmpoXCJcbiAgICAgICAgICAgICAgY3Jvc3NPcmlnaW49XCJhbm9ueW1vdXNcIi8+XG4gICAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgICAgIC5idXR0b257XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICAgICAgICB9YH1cbiAgICAgICAgPC9zdHlsZT5cbiAgICA8L2hlYWQ+XG4gICAgPGJvZHk+XG5cbiAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbGctMyBjb2wtbWQtMyBjb2wtc20tMCBjb2wteHMtMFwiPjwvZGl2PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtbGctNiBjb2wtbWQtNiBjb2wtc20tMTIgY29sLXhzLTEyXCI+XG5cbiAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICA8aDI+Q291bnRyeTwvaDI+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cIlwiLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgIHtwcm9wcy5jb3VudHJpZXMubWFwKHNob3cgPT4gKFxuXG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTQgY29sLW1kLTQgY29sLXNtLTEyIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYz1cImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBT0VBQUFEaENBTUFBQUFKYlNKSUFBQUFmbEJNVkVWV1BYei8vLzlUT1hyeThQVkZKSEZvVW9wdVdvMXdYbzVLTEhSUE0zZjkvUDJmbExKMFlaTEZ2dEhKdzlSUU5YaExMblZJS1hOYlE0RGExZUhtNCt1dnBiK0NjWjJhanE5clZveGVSb0x1N1BLRWRaNzM5dm5UemR4aFNvU1RocW02c3NoQkhtNm5uTG1IZUtDenFjSjdhcGpWME4yL3VNemgzZWVOZjZXTGY2K0ZBQUFIdUVsRVFWUjRuTzJkZjVPcUxCU0FBYzBMYVNGcGFhWG1kcmNmKy8yLzRLdDEyNjBOM1BZTlBNajAvTnNNMHpNZ0tKeHpRTGdMcjZ5elJWRE5rSjNNcW1BUjFxWFg2WUNVdnhSbGxnc2FjMEtnUlRvZ2hNZlV6N01PUzVWaHNsa0ticlBiTllTTDJTYjVsZUUwajlsUTlNNFFGbyttRHh0T2N6RXN2VE5FVkRMSGU4TjBOMGkvRmlKMjZjK0djODZoLytnVGNEYi93ZENMS1BTZmZKSTQ4cm9NRXpMa0RqekRTYUkybkF4c0FwVkQ0b25Lc0JiUWYwNFR0SlliMW0vUS8wd2JiN1hNY0RMME9lWWFPcmszVEJqMHY5SUtTNzRiZWxhL1lQOGVncnh2aHRId2w0bGJlSFJyT0kraC81RjI0dm0xWWVyV1EzaUdwMWVHTzlmR2FBdmZmUmxPWFZucWJ4SFRUOE9SVy9Qb0JUSzZHRHJhaGY4NnNUWE0zZXpDcGhQenMySGkza3B4SVU1T2hoc1hsNG96Zk5NYUZrdFhCMmt6VEpkRlkxaTZPcyswaUxJeHpGeGM3Uyt3ckRGMGRpWnRhV1pUNUxrOFNCSHlQVlM2OUdsL0R5MVI3ZTVxMkJMWEtIUjVvbW1tbWhBdFhKNW9tcWxtZ1FMSERRTlVRZjhIdzFUSTFqTjZYYmp1OStMRml4Y3ZYcng0OFFCRUE5QU9uWkRnU2ZKUlZjMjRFREd6TkE1U3JNWlBVaFNGdDByM2grM0hTRkFMbzNuODd1amMzMUdVMjF6RWxrbHFOVHhaSG5iY3FwN1VidGlRYml0cWo2TUp3NllqNjVrMSszOW1EQnUyekpJZFFHT0dlTFcyWTZQYW5DSEd0UlhUcWtsRGZKeFpNRktOR3VJaWdEK2NObXVJeHpsNEx4bzJ4RjRGL1N5YU5zUXBkQ2NhTjhRVDRMWGZ2Q0Zldy9aaUQ0YWU3N29oM29LTzB6NE1QZERwVkdWWVRoOWlueHhYUHl0bWtPdSt5bkFwNkNNSXl0Qm9YZjlnQ1JwWXFETDhSVVF1NGJHZkg4WmRpcERuOEJvTVd4VEpueGUyZ01OVWsySHJ1RkViN2dFL0ZiVVpJc1IyeXBFNkJud1FOUm9pOWxmWmlZQ3hoVG9Oa1pqSUc4TjRBL2ZtcHRXUVZLcHhDaGg2cDlYd09tZndscWtyaG55aE1FemdRbUQxR2lLaWFDNTF4bERzNWMwQmZrRnBOb3p2NmlHNFpzZ3llWE9GTTRhcXFjYWRQaVNCdkxtVk16UE5PVi93bnFNN2hwVzhPY0NFaVo3NjhPREtPNDN5T1FUTXpOSTlseW8rb0FDM01YU3ZoNkcwTlZlK2dCdmlnN1MxeEkxZGpCWWgzMWVFM0REVmEwZ2lhV05qeUdvQmVnM2pkMmxqb09tRGVuY3hab1cwTWREek5iMDdVYlcwTGNBUGZLVFhrTXVmUXVDeUt4b052Nm9lM1FKY3MwT2ZJYS9rSzBVQkhDV3R6WkJHaWlPMk5YRFFrQ1pEN3N0ZjE2Q1B1SkVlUThMbytyN282SmtEZUswQXBTRi9MQkdCTThyeVVPV0hKL0JsRGxXR2krZ1Jkb3ROT0ZIcVlWeGJFR0txak1Yb1BMVitrQXo2R1d3eEdHMnlDbXdRTkdoNHNLU2FzU25EWkdmQkkzakNqT0Z4YlVXSTl3a2poaHZmdHlkdHhrd2Zyc3BzSk94NERNM05OT1A5aHc4ZnhvN014aWF1c3RpQ2ZqUWJmYm42QzEvZzMzUjg2UlE4cWNSNEJLMFh1Ui9Kdm9GZCsvdUlnczVBRmZzd2hQM0c2TVVRN3l3OHQ5aUdqN0tkMStWS3Z0Zjl5WGhtMy9saHhSNG5wbUs1ZnUvNDFNZDRQL2hJQmNKanR1dUs5SWFya3F0eno1dFdxaXVwTUM3QXhxbmVzeWZscmluRzcxRHpxZTVUYm1VME8xaXRZODJHaUFVcVJhaTdHM1FiSXFZS0U0WUtUOVJ1aUtnaXhCUi93SHhsNkRkRVRIRUlCWFNPYU1DUWZ5aUdLY3hVWThBUXZTazZFV2FZbWpDOHYvenVETXlTYU1Md2RHT0RoQ1BJZDZJSlE4VGx3OVFES1NacnhKQ1c4a1pCWWpDTkdLcXlMdjVDVERWbVJxa2k2eUtFK0lReVk2aUlGQWFaVE0wWXJ1V05ndVRvR1RFa08zbWpJQW5QWmd3VmZiaDNwZzlWenlGSWJRVXpob3JVL0tNemhtenJlaDlTeGI2aU80Wk1zVHZzekZ4S2x2STJZVzd6TmZKOXFLb0JNbmZscmMxWGJYMkQxSTR3WUtqS1FjUTRjdVRyeVZlZDBIZ2cyOTc2RGJuaXBkU1pYUXhDbEFlSmp1eEVDZlVwSWt6NmsyN0ROM21LWlVzQmMwaXFPZitRZFp3REErVnpheldrd1ZFdENMTlc2RFFrTVZHZE9wMUlCMzI2Um5oTXE3bzc1QVRvY08zWnVvazBwbFF3bEsvcnJ2RjU2a0tvWUF5VjRYVHlJTk15U1I4b2Z3bldoVDFGZlVFbU8vZGtXQ0N3c0srZURBSFRMUHN4bkZ0WU5VSXJJTHNYZlJwT1FNdGQ5MkI0Y0wyZTl4WTRpYzIwWWJHQXp0SXpiRmdTOE53bm80YXJqUVgzUGhrMEhHL2hPeEFaTkN6bTNJcEVaMU9HYWNqczhETmp1S3FqMklieGVVYTdZZm9lRVh2U25KRld3M0dSMUIvRXQwcXZ3ZjhobitjQkwyK1ZIcWZ2NFRxUGhRMVpzZDhod1o4bk9GMURPaU9ObXJYWGtPcTRTeGJhNE1XTEZ5OWVQQVJJQ0h5UHpGQUYvUmNNVXlISSs2SjZnQVJvNGJqaEFvVVd2dTlxaElVSThDS2VQb2hyQkZxSTJEeTBSQjU0ZlVtakNBOUIzdmxsSHBKakJIdEhwR2xZMWhpV0xnOVRVVGFHWVBVS2VvQXNpOFlRc0hLSWNkcnNGQVI4bGFsWjR1Ums2TzVzZXNxYWJnMkI2NTZiNHhTUTJ4bytsK05pTCtlNC81T2hvNTE0anFrK0dRSmZRR0NJZjllNG5RMVRKdzNUSzBQUXlDcERYRXJkL0RQRWtXdTkrSGtqeU1YUWMreVE1T3RHa0lzaFR0eDZkMk9mS1hDZmhyQXhnTHE1dWlENnl4RFg4SmN0Nk9MdDZ0NmhLME5jdzFjbTFnTzl2bGpwMmhCUDdDbUcvZ1NFM1Z4L2ZXT0lFelQ4UllPVDJ6emJXMFBzUlVPZmIyajBMUVRtbTJIemRtUEp6UkwvRDg3djBxZnVESEc2R095RVE4VHVQZ2YxM3JENW1Cb04wcEdJa1N5QlVXYllPQWJVMmhnZU9ZVEh1VHhCVTI3WXpLcWJTZ3htN1NCTUxEZXFTZ1VxUTR5TE1zdDlHak9yWDhrSmFZdEpCMW1wanJCVEc3WjRaUjB1Z3NyV3MvNVpGU3pDdXV5T2tQd1BNbG1BTnNkcjY2SUFBQUFBU1VWT1JLNUNZSUk9XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cImhlaWdodDogMTAwJTt3aWR0aDogMTAwJTtcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLWxnLTggY29sLW1kLTggY29sLXNtLTEyIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+PGI+Q291bnRyeSA6IEluZGlhPC9iPjwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPjxiPkN1cnJlbnQgdGltZSBhbmQgZGF0ZSA6IDEybXQsIGlzZWYgLCB1Z2l3dWUuPC9iPjwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPjxiPkNvdW50cnkgOiBJbmRpYTwvYj48L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICkpfVxuXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbGctMyBjb2wtbWQtMyBjb2wtc20tMCBjb2wteHMtMFwiPjwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cblxuICAgIDwvYm9keT5cbiAgICA8L2h0bWw+XG4pO1xuXG5JbmRleC5nZXRJbml0aWFsUHJvcHMgPSBhc3luYyBmdW5jdGlvbiAoKSB7XG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2goJ2h0dHBzOi8vcmVzdGNvdW50cmllcy5ldS9yZXN0L3YyL2FsbCcpO1xuICAgIGNvbnN0IGRhdGEgPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgY29uc29sZS5sb2coYFNob3cgZGF0YSBmZXRjaGVkLiBDb3VudDogJHtkYXRhLmxlbmd0aH1gKTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIGNvdW50cmllczogZGF0YS5tYXAoZW50cnkgPT4gZW50cnkpXG4gICAgfTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEluZGV4O1xuIl19 */\n/*@ sourceURL=/home/zed/Documents/CountryDataProject/pages/index.js */")), __jsx("body", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, __jsx("div", {
    className: "container-fluid",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx("div", {
    className: "row",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, __jsx("div", {
    className: "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }), __jsx("div", {
    className: "col-lg-6 col-md-6 col-sm-12 col-xs-12",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, __jsx("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "Country"), __jsx("input", {
    type: "text",
    className: "",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  })), props.countries.map(function (show) {
    return __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    }, __jsx("div", {
      "class": "container-fluid",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    }, __jsx("div", {
      "class": "row",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, __jsx("div", {
      "class": "col-lg-4 col-md-4 col-sm-12 col-xs-12",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, __jsx("img", {
      src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAflBMVEVWPXz///9TOXry8PVFJHFoUopuWo1wXo5KLHRPM3f9/P2flLJ0YZLFvtHJw9RQNXhLLnVIKXNbQ4Da1eHm4+uvpb+CcZ2ajq9rVoxeRoLu7PKEdZ739vnTzdxhSoSThqm6sshBHm6nnLmHeKCzqcJ7apjV0N2/uMzh3eeNf6WLf6+FAAAHuElEQVR4nO2df5OqLBSAAc0LaSFpaaXmdrcf+/2/4Kt1260N3PYNPMj0/NsM0zMgKJxzQLgLr6yzRVDNkJ3MqmAR1qXX6YCUvxRllgsac0KgRToghMfUz7MOS5VhslkKbrPbNYSL2Sb5leE0j9lQ9M4QFo+mDxtOczEsvTNEVDLHe8N0N0i/FiJ26c+Gc86h/+gTcDb/wdCLKPSffJI48roMEzLkDjzDSaI2nAxsApVD4onKsBbQf04TtJYb1m/Q/0wbb7XMcDL0OeYaOrk3TBj0v9IKS74bela/YP8egrxvhtHwl4lbeHRrOI+h/5F24vm1YerWQ3iGp1eGO9fGaAvffRlOXVnqbxHTT8ORW/PoBTK6GDrahf86sTXM3ezCphPzs2Hi3kpxIU5OhhsXl4ozfNMaFktXB2kzTJdFY1i6Os+0iLIxzFxc7S+wrDF0diZtaWZT5Lk8SBHyPVS69Gl/Dy1R7e5q2BLXKHR5ommmmhAtXJ5omqlmgQLHDQNUQf8Hw1TI1jN6Xbju9+LFixcvXrx48QBEA9AOnZDgSfJRVc24EDGzNA5SrMZPUhSFt0r3h+3HSFALo3n87ujc31GU21zElklqNTxZHnbcqp7UbtiQbitqj6MJw6Yj65k1+39mDBu2zJIdQGOGeLW2Y6PanCHGtRXTqklDfJxZMFKNGuIigD+cNmuIxzl4Lxo2xF4F/SyaNsQpdCcaN8QT4LXfvCFew/ZiD4ae77oh3oKO0z4MPdDpVGVYTh9inxxXPytmkOu+ynAp6CMIytBoXf9gCRpYqDL8RUQu4bGfH8ZdipDn8BoMWxTJnxe2gMNUk2HruFEb7gE/FbUZIsR2ypE6BnwQNRoi9lfZiYCxhToNkZjIG8N4A/fmptWQVKpxChh6p9XwOmfwlqkrhnyhMEzgQmD1GiKiaC51xlDs5c0BfkFpNozv6iG4ZsgyeXOFM4aqqcadPiSBvLmVMzPNOV/wnqM7hpW8OcCEiZ768ODKO43yOQTMzNI9lyo+oAC3MXSvh6G0NVe+gBvig7S1xI1djBYh31eE3DDVa0giaWNjyGoBeg3jd2ljoOmDencxZoW0MdDzNb07UbW0LcAPfKTXkMufQuCyKxoNv6oe3QJcs0OfIa/kK0UBHCWtzZBGiiO2NXDQkCZD7stf16CPuJEeQ8Lo+r7o6JkDeK0ApSF/LBGBM8ryUOWHJ/BlDlWGi+gRdotNOFHqYVxbEGKqjMXoPLV+kAz6GWwxGG2yCmwQNGh4sKSasSnDZGfBI3jCjOFxbUWI9wkjhhvftydtxkwfrspsJOx4DM3NNOP9hw8fxo7MxiaustiCfjQbfbn6C1/g33R86RQ8qcR4BK0XuR/JvoFd+/uIgs5AFfswhP3G6MUQ7yw8t9iGj7Kd1+VKvtf9yXhm3/lhxR4npmK5fu/41Md4P/hIBcJjtuuK9Iarkqtzz5tWqiupMC7AxqnesyflrinG71Dzqe5TbmU0O1itY82GiAUqRai7G3QbIqYKE4YKT9RuiKgixBR/wHxl6DdETHEIBXSOaMCQfyiGKcxUY8AQvSk6EWaYmjC8v/zuDMySaMLwdGODhCPId6IJQ8Tlw9QDKSZrxJCW8kZBYjCNGKqyLv5CTDVmRqki6yKE+IQyY6iIFAaZTM0YruWNguToGTEkO3mjIAnPZgwVfbh3pg9VzyFIbQUzhorU/KMzhmzreh9Sxb6iO4ZMsTvszFxKlvI2YW7zNfJ9qKoBMnflrc1XbX2D1I4wYKjKQcQ4cuTryVed0Hgg2976DbnipdSZXQxClAeJjuxECfUpIkz6k27DN3mKZUsBc0iqOf+QdZwDA+VzazWkwVEtCLNW6DQkMVGdOp1IB326RnhMq7o75ATocO3Zuok0plQwlK/rrvF56kKoYAyV4XTyINMySR8ofwnWhT1FfUEmO/dkWCCwsK+eDAHTLPsxnFtYNUIrILsXfRpOQMtd92B4cL2e9xY4ic20YbGAztIzbFgS8Nwno4arjQX3Phk0HG/hOxAZNCzm3IpEZ1OGacjs8DNjuKqj2IbxeUa7YfoeEXvSnJFWw3GR1B/Et0qvwf8hn+cBL2+VHqfv4TqPhQ1Zsd8hwZ8nOF1DOiONmrXXkOq4Sxba4MWLFy9ePARICHyPzFAF/RcMUyHI+6J6gARo4bjhAoUWvu9qhIUI8CKePohrBFqI2Dy0RB54fUmjCA9B3vllHpJjBHtHpGlY1hiWLg9TUTaGYPUKeoAsi8YQsHKIcdrsFAR8lalZ4uRk6O5sesqabg2B656b4xSQ2xo+l+NiL+e4/5Oho514jqk+GQJfQGCIf9e4nQ1TJw3TK0PQyCpDXErd/DPEkWu9+HkjyMXQc+yQ5OtGkIshTtx6d2OfKXCfhrAxgLq5uiD6yxDX8Jct6OLt6t6hK0Ncw1cm1gO9vljp2hBP7CmG/gSE3Vx/fWOIEzT8RYOT2zzbW0PsRUOfb2j0LQTmm2HzdmPJzRL/D87v0qfuDHG6GOyEQ8TuPgf13rD5mBoN0pGIkSyBUWbYOAbU2hgeOYTHuTxBU27YzKqbSgxm7SBMLDeqSgUqQ4yLMst9GjOrX8kJaYtJB1mpjrBTG7Z4ZR0ugsrWs/5ZFSzCuuyOkPwPMlmANsdr66IAAAAASUVORK5CYII=",
      style: "height: 100%;width: 100%;",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    })), __jsx("div", {
      "class": "col-lg-8 col-md-8 col-sm-12 col-xs-12",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41
      },
      __self: this
    }, __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 42
      },
      __self: this
    }, "Country : India")), __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 43
      },
      __self: this
    }, "Current time and date : 12mt, isef , ugiwue.")), __jsx("p", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, __jsx("b", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44
      },
      __self: this
    }, "Country : India"))))));
  })), __jsx("div", {
    "class": "col-lg-3 col-md-3 col-sm-0 col-xs-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  })))));
};

Index.getInitialProps = function _callee() {
  var res, data;
  return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_4___default()('https://restcountries.eu/rest/v2/all'));

        case 2:
          res = _context.sent;
          _context.next = 5;
          return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(res.json());

        case 5:
          data = _context.sent;
          console.log("Show data fetched. Count: ".concat(data.length));
          return _context.abrupt("return", {
            countries: data.map(function (entry) {
              return entry;
            })
          });

        case 8:
        case "end":
          return _context.stop();
      }
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ })

})
//# sourceMappingURL=index.js.54272f3b6deafc437ca2.hot-update.js.map