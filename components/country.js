import Link from 'next/link';

const buttonClassStyles = {
    width: '100%',
    padding: '10px',
    border: '2px solid #495ad8',
    color:'#495ad8',
    backgroundColor:'#fff',
    display:'block',
    textAlign:'center'
};
const img = {
    height: '100%',
    width: '100%'
};
const ul = {
    paddingLeft: '0'
};
const li = {
    marginTop: '20px',
    padding: '10px',
    listStyleType: 'none',
    boxShadow: 'rgb(193, 193, 193) 0px 0px 2px 1px'
};
const h1 = {
    fontWeight: 'bolder'
};

function dateFormat(timezone) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let dates = ['1st', '2nd', '3rd', '4th','5th', '6th','7th', '8th', '9th', '10th', '11th', '12th', '13th', '14th', '15th', '16th', '17th', '18th', '19th', '20th', '21st', '22nd', '23rd', '24th', '25th', '26th', '27th', '28th', '29th', '30th', '31st'];
    let d = new Date().addHours(parseFloat(timezone.replace("UTC", "")));
    return dates[d.getDate() - 1]+' '+months[d.getMonth() - 1]+' '+d.getFullYear()+', '+d.getHours()+':'+d.getMinutes(); //'23rd Jan 2020, 14:20'
};

const CountryTemplate = props => (
    <li style={li}>
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <img src={props.country.flag} style={img}/>
                    </div>
                    <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <h1 style={h1}>{props.country.name}</h1>
                        <p><b>Currency : {props.country.currencies[0].name}</b></p>
                        <p><b>Current date and time : {dateFormat(props.country.timezones[0])}</b></p>
                        <a href={'http://www.google.com/maps/place/'+props.country.latlng[0]+','+props.country.latlng[1]} style={buttonClassStyles} target="_blank">Show Map</a>
                    </div>
                </div>
            </div>
        </div>
    </li>
);
export default function Country(props) {
    Date.prototype.addHours= function(h) {
        this.setHours(this.getHours()+h);
        return this;
    };
    return (
        <ul style={ul}>
            {props.countries.map(country => (
                <CountryTemplate key={country.name.toString()} country={country}/>
            ))}
        </ul>
    );
}
