import ReactDOM from 'react-dom';
import CountriesData from '../components/country'
import fetch from 'isomorphic-unfetch';

var countries = [];
const h1 = {
    fontWeight: 'bolder'
};

const input = {
    width: '100%',
    padding: '10px',
    backgroundImage: 'url(https://www.thesirona.com/themes/storefront/public/images/search.png?v=1.1.5)',
    backgroundSize: '25px 25px',
    backgroundPosition: '98% center',
    backgroundRepeat: 'no-repeat',
    border: '1px solid #ccc'
};

const Index = props => (
    <div>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossOrigin="anonymous"/>
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-3 col-md-3 col-sm-0 col-xs-0"> </div>
                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div>
                        <h1 style={h1}>Country</h1>
                        <input type="text" style={input} onInput={e => Index.searchCountry(e.currentTarget.value)} placeholder="Search countries"/>
                    </div>
                    <div id="dataDump">
                        <CountriesData countries={props.countries} />
                    </div>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-0 col-xs-0"> </div>
            </div>
        </div>
    </div>
);
Index.getInitialProps = async function () {
    const res = await fetch('https://restcountries.eu/rest/v2/all');
    const data = await res.json();
    countries = data.map(entry => entry);
    return {
        countries : countries
    };
};
Index.searchCountry = value => {
    const filtered = (arr, query) => {
        return arr.filter(el => el.name.toLowerCase().includes(value.toLowerCase()))
    };
    Index.render(filtered(countries, value));
};

Index.render = value => {
    ReactDOM.render(
        <CountriesData countries={value} />,
        document.getElementById('dataDump')
    );
};

Index.getInitialProps();
export default Index;
